package ru.zak.animals;

/**
 * author Заставская Анастасия 15oit18
 */
public class Cat extends Animal {

    public Cat(){
        this("Кошка"," мяу-мяу");
    }


    protected Cat(String name, String voice) {
        super(name, voice);
    }
}
