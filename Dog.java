package ru.zak.animals;


public class Dog extends Animal {


    public Dog() {
        this("Собака", "гав-гав");
    }


    protected Dog(String name, String voice) {
        super(name, voice);
    }

}
