package ru.zak.animals;

public class Main {

    public static void main(String... args) {
        Dog dog = new Dog();
        Dog dog1 = new Dog("Собака", "гав-гав");
        Dog dog2 = new Dog("Тузик", "тяф-тяф");


        dog.printDisplay();
        dog1.printDisplay();
        dog2.printDisplay();


        Cat cat = new Cat();
        Cat cat1 = new Cat("Кошка","мяу-мяу");
        Cat cat2 = new Cat("Миля","мя-мяу");

        cat.printDisplay();
        cat1.printDisplay();
        cat2.printDisplay();


        Tiger tiger = new Tiger();
        Tiger tiger1 = new Tiger("Тигр","мя-ррр");
        Tiger tiger2 = new Tiger("Киса","рррр-мяу");

        tiger.printDisplay();
        tiger1.printDisplay();
        tiger2.printDisplay();


    }
}