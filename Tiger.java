package ru.zak.animals;

/**
 * @author Заставская Анастасия
 */
public class Tiger extends Animal {

    public Tiger(){
        this("Тигр","мя-ррр");
    }

    protected Tiger(String name, String voice) {
        super(name, voice);
    }
}

